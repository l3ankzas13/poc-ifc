import './App.css';
import { IfcViewerAPI } from 'web-ifc-viewer';
import { IconButton } from '@material-ui/core';
import React from 'react';
import Dropzone from 'react-dropzone';

//Icons
import FolderOpenOutlinedIcon from '@material-ui/icons/FolderOpenOutlined';

class App extends React.Component {

    state = {
        bcfDialogOpen: false,
        loaded: false,
        loading_ifc: false
    };

    constructor(props) {
        super(props);
        this.dropzoneRef = React.createRef();
    }

    async componentDidMount() {
        const container = document.getElementById('viewer-container');
        const viewer = new IfcViewerAPI({container});
        // viewer.addAxes();
        // viewer.addGrid();
        viewer.IFC.setWasmPath('../../');

        this.viewer = viewer;

        await this.viewer.IFC.loadIfcUrl('https://firebasestorage.googleapis.com/v0/b/nontananfilmmaker-ceb05.appspot.com/o/ifc%2FUnit_1BD_35_A%5B1%5D.IFC?alt=media&token=d34c1f30-72f3-4f8f-969d-3de0ba6bbfbb', true);

        window.onmousemove = viewer.prepickIfcItem;
        window.ondblclick = viewer.addClippingPlane
    }

    onDrop = async (files) => {
        this.setState({ loading_ifc: true })
        console.log('files[0]', files[0]);
        await this.viewer.IFC.loadIfcUrl('https://firebasestorage.googleapis.com/v0/b/nontananfilmmaker-ceb05.appspot.com/o/ifc%2FUnit_1BD_35_A%5B1%5D.IFC?alt=media&token=d34c1f30-72f3-4f8f-969d-3de0ba6bbfbb', true);
        this.setState({ loaded: true, loading_ifc: false })
    };


    handleClickOpen = () => {
        this.dropzoneRef.current.open();
    };

    render() {
        return (
          <>
            <IconButton onClick={this.handleClickOpen}>
                <FolderOpenOutlinedIcon onClick={this.handleClickOpen} />
            </IconButton>
              <div style={{ display: 'flex', flexDirection: 'row', height: '100vh' }}>
                <Dropzone ref={this.dropzoneRef} onDrop={this.onDrop}>
                    {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: 'dropzone' })}>
                        <input {...getInputProps()} />
                    </div>
                    )}
                </Dropzone>
                <div style={{ flex: '1 1 auto', minWidth: 0 }}>
                    <div id='viewer-container' style={{ position: 'relative', height: '100%', width: '100%' }} />
                </div>
              </div>
          </>
        );
    }
}

export default App;
